package me.noan.tn.brainteaser.api;

import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import me.noan.dev.tn_test.brain_teaser.api.server.model.AttemptRequestDto;
import me.noan.dev.tn_test.brain_teaser.api.server.model.AttemptResultDte;
import me.noan.tn.brainteaser.domain.Attempt;
import me.noan.tn.brainteaser.domain.EvaluationResult;
import me.noan.tn.brainteaser.domain.IntegerEvaluator;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.LinkedHashSet;
import java.util.List;

import static jakarta.validation.Validation.buildDefaultValidatorFactory;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class AttemptMapperTest {
    @BeforeAll
    static void beforeAll() {
        Attempt.evaluator = mock(IntegerEvaluator.class);
    }

    Validator validator;

    @BeforeEach
    void setUp() {
        try (ValidatorFactory factory = buildDefaultValidatorFactory()) {
            validator = factory.getValidator();
        }
    }

    @Test
    void convertToDtoOk() {
        when(Attempt.evaluator.evaluate(any())).thenReturn(EvaluationResult.CORRECT);

        var expected = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9);
        var input = new Attempt(14L, new LinkedHashSet<>(expected));
        var actual = AttemptMapper.toDto(input);
        assertThat(actual.getId()).isEqualTo(14L);
        assertThat(actual.getElements().stream().toList()).isEqualTo(expected);
        assertThat(actual.getResult()).isEqualTo(AttemptResultDte.CORRECT);
        assertThat(validator.validate(actual)).isEmpty();
    }

    @Test
    void convertCorrectResult() {
        var input = EvaluationResult.CORRECT;
        var actual = AttemptMapper.toDto(input);
        assertThat(actual).isEqualTo(AttemptResultDte.CORRECT);
        assertThat(validator.validate(actual)).isEmpty();
    }

    @Test
    void convertIncorrectResult() {
        var input = EvaluationResult.INCORRECT;
        var actual = AttemptMapper.toDto(input);
        assertThat(actual).isEqualTo(AttemptResultDte.INCORRECT);
        assertThat(validator.validate(actual)).isEmpty();
    }

    @Test
    void convertToModel() {
        var expected = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9);
        var input = new AttemptRequestDto().elements(new LinkedHashSet<>(expected));
        var actual = AttemptMapper.toModel(input);
        assertThat(actual.getCoefficients().stream().toList()).isEqualTo(expected);
        assertThat(validator.validate(actual)).isEmpty();
    }

    @Test
    void updateModel() {
        var expected = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9);
        var input = new AttemptRequestDto().elements(new LinkedHashSet<>(expected));
        var actual = AttemptMapper.updateModel(14L, input);
        assertThat(actual.getCoefficients().stream().toList()).isEqualTo(expected);
        assertThat(actual.getIdentifier()).isEqualTo(14L);
        assertThat(validator.validate(actual)).isEmpty();
    }

    @Test
    void convertToDtoOkButBadTest() {
        var expected = List.of(1, 2, 3, 4, 5, 6, 7, 8, 20);
        var input = new Attempt(new LinkedHashSet<>(List.of(1, 2, 4, 3, 5, 6, 7, 8, 9)));
        var actual = AttemptMapper.toDto(input);
        assertThat(actual.getElements().stream().toList())
                .isEqualTo(List.of(1, 2, 4, 3, 5, 6, 7, 8, 9));
    }

    // DTO validation tests
    @Test
    void convertToDtoElementOutOfUpperBound() {
        var expected = List.of(1, 2, 3, 4, 5, 6, 7, 8, 20);
        var input = new Attempt(new LinkedHashSet<>(expected));
        var actual = AttemptMapper.toDto(input);
        assertThat(actual.getElements().stream().toList()).isEqualTo(expected);
        assertThat(validator.validate(actual)).isNotEmpty();
    }

    @Test
    void convertToDtoElementOutOfLowerBound() {
        var expected = List.of(1, 2, 3, 4, 5, 6, 7, 8, 0);
        var input = new Attempt(new LinkedHashSet<>(expected));
        var actual = AttemptMapper.toDto(input);
        assertThat(actual.getElements().stream().toList()).isEqualTo(expected);
        assertThat(validator.validate(actual)).isNotEmpty();
    }

    @Test
    void convertToDtoUndersized() {
        var expected = List.of(1, 2, 3, 4, 5, 6, 7, 8);
        var input = new Attempt(new LinkedHashSet<>(expected));
        var actual = AttemptMapper.toDto(input);
        assertThat(actual.getElements().stream().toList()).isEqualTo(expected);
        assertThat(validator.validate(actual)).isNotEmpty();
    }

    @Test
    void convertToDtoOversized() {
        var expected = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        var input = new Attempt(new LinkedHashSet<>(expected));
        var actual = AttemptMapper.toDto(input);
        assertThat(actual.getElements().stream().toList()).isEqualTo(expected);
        assertThat(validator.validate(actual)).isNotEmpty();
    }
}