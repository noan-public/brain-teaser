package me.noan.tn.brainteaser.domain;

import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import me.noan.dev.tn_test.brain_teaser.api.server.model.AttemptRequestDto;
import me.noan.tn.brainteaser.api.AttemptMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.LinkedHashSet;
import java.util.List;

import static jakarta.validation.Validation.buildDefaultValidatorFactory;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class AttemptTest {
    Validator validator;

    @BeforeEach
    void setUp() {
        Attempt.evaluator = mock(IntegerEvaluator.class);
        try (ValidatorFactory factory = buildDefaultValidatorFactory()) {
            validator = factory.getValidator();
        }
    }

    @Test
    void uninitializedEvaluator() {
        Attempt.evaluator = null;
        var attempt = new Attempt(new LinkedHashSet<>(List.of(9, 8, 7, 6, 5, 4, 3, 2, 1)));
        assertThatThrownBy(attempt::getEvaluationResult).isInstanceOf(UninitializedContextException.class);
    }

    @Test
    void successfulEvaluation() {
        when(Attempt.evaluator.evaluate(any())).thenReturn(EvaluationResult.CORRECT);
        var attempt = new Attempt(new LinkedHashSet<>(List.of(9, 8, 7, 6, 5, 4, 3, 2, 1)));
        assertThat(attempt.getEvaluationResult()).isEqualTo(EvaluationResult.CORRECT);
    }

    @Test
    void failedEvaluation() {
        when(Attempt.evaluator.evaluate(any())).thenReturn(EvaluationResult.INCORRECT);
        var attempt = new Attempt(new LinkedHashSet<>(List.of(9, 8, 7, 6, 5, 4, 3, 2, 1)));
        assertThat(attempt.getEvaluationResult()).isEqualTo(EvaluationResult.INCORRECT);
    }

    @Test
    void undersizedCoefficientList() {
        var attempt = new Attempt(new LinkedHashSet<>(List.of(9, 8, 7, 6, 5, 4, 2, 1)));
        assertThat(validator.validate(attempt)).isNotEmpty();
    }

    @Test
    void oversizedCoefficientList() {
        var attempt = new Attempt(new LinkedHashSet<>(List.of(9, 8, 7, 6, 5, 4, 3, 2, 1, 0)));
        assertThat(validator.validate(attempt)).isNotEmpty();
    }

    @Test
    void duplicatedCoefficientValue() {
        var attempt = new Attempt(new LinkedHashSet<>(List.of(9, 8, 7, 6, 6, 4, 3, 2, 1)));
        assertThat(validator.validate(attempt)).isNotEmpty();
    }

    @Test
    void outOfUpperBoundCoefficientValue() {
        var attempt = new Attempt(new LinkedHashSet<>(List.of(9, 8, 7, 6, 0, 4, 3, 2, 1)));
        assertThat(validator.validate(attempt)).isNotEmpty();
    }

    @Test
    void outOfLowerBoundCoefficientValue() {
        var attempt = new Attempt(new LinkedHashSet<>(List.of(9, 8, 7, 6, 10, 4, 3, 2, 1)));
        assertThat(validator.validate(attempt)).isNotEmpty();
    }
}