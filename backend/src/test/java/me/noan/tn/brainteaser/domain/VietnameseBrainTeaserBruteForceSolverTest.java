package me.noan.tn.brainteaser.domain;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class VietnameseBrainTeaserBruteForceSolverTest {
    @Test
    void solve() {
        var solver = new VietnameseBrainTeaserBruteForceSolver();
        solver.solve();
        assertThat(solver.getSolutions()).isNotEmpty();
        assertThat(solver.getLastExecutionTimeInMs()).isGreaterThan(0);
    }
}