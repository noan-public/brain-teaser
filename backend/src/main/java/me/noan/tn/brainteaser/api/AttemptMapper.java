package me.noan.tn.brainteaser.api;

import jakarta.validation.Valid;
import me.noan.dev.tn_test.brain_teaser.api.server.model.*;
import me.noan.tn.brainteaser.domain.Attempt;
import me.noan.tn.brainteaser.domain.EvaluationResult;
import me.noan.tn.brainteaser.domain.EvaluationResultSelector;
import org.springframework.data.domain.Page;

import java.util.LinkedHashSet;

public class AttemptMapper {
    public static Attempt toModel(@Valid AttemptRequestDto dto) {
        return new Attempt(new LinkedHashSet<>(dto.getElements()));
    }

    public static Attempt updateModel(Long id, @Valid AttemptRequestDto dto) {
        return new Attempt(id, new LinkedHashSet<>(dto.getElements()));
    }

    public static AttemptResponseDto toDto(@Valid Attempt model) {
        var dto = new AttemptResponseDto();
        dto.setElements(model.getCoefficients());
        dto.setResult(toDto(model.getEvaluationResult()));
        dto.setId(model.getIdentifier());
        return dto;
    }

    public static AttemptResultDte toDto(EvaluationResult model) {
        if (model == EvaluationResult.CORRECT) return AttemptResultDte.CORRECT;
        return AttemptResultDte.INCORRECT;
    }

    public static EvaluationResultSelector toModel(AttemptResultFilterDte dto) {
        if (dto == AttemptResultFilterDte.ALL) return EvaluationResultSelector.ALL;
        else if (dto == AttemptResultFilterDte.CORRECT) return EvaluationResultSelector.CORRECT;
        else return EvaluationResultSelector.INCORRECT;
    }

    public static AttemptPageDto toDto(Page<Attempt> model) {
        var dto = new AttemptPageDto();
        dto.setCurrentPageIndex(model.getNumber());
        dto.setAttempts(model.getContent().stream().map(AttemptMapper::toDto).toList());
        dto.setTotalItemCount(model.getTotalElements());
        dto.setTotalPageCount(model.getTotalPages());
        return dto;
    }
}
