package me.noan.tn.brainteaser.domain;

import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.hibernate.validator.constraints.Range;

import java.util.LinkedHashSet;
import java.util.SequencedCollection;
import java.util.SequencedSet;

@AllArgsConstructor
@Getter
public class Attempt {
    /**
     * Evaluator evaluates if the attempt is successful or not.
     * Attempt evaluator can be set by application context.
     * No use of SpringBoot mechanism here, because we want domain to be independent of
     * underlying infra technology.
     */
    public static Evaluator<Integer> evaluator;

    public Attempt(SequencedCollection<Integer> coefficients) {
        this.coefficients = new LinkedHashSet<>(coefficients);
    }

    private Long identifier;

    @Size(min = 9, max = 9, message = "exactly 9 coefficients are expected")
    private SequencedSet<@Range(min = 1, max = 9) Integer> coefficients;

    /**
     * Evaluates attempt coefficients correctness.
     * @return Correct if evaluation succeeded, Incorrect else
     */
    public EvaluationResult getEvaluationResult() {
        if (evaluator == null)
            throw new UninitializedContextException("Attempt evaluator must be initialized");

        return evaluator.evaluate(coefficients);
    }
}
