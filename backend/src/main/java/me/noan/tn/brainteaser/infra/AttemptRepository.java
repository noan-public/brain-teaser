package me.noan.tn.brainteaser.infra;

import me.noan.tn.brainteaser.domain.Attempt;
import me.noan.tn.brainteaser.domain.EvaluationResultSelector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Component
public class AttemptRepository implements me.noan.tn.brainteaser.domain.AttemptRepository {
    @Autowired
    AttemptDsRepository attemptDsRepository;

    @Override
    public Attempt create(Attempt record) {
        var entity = new AttemptEntity(record);
        return attemptDsRepository.save(entity).toDomain();
    }

    @Override
    public boolean exists(Long id) {
        return attemptDsRepository
                .existsById(id);
    }

    @Override
    public Optional<Attempt> get(Long id) {
        return attemptDsRepository
                .findById(id)
                .map(AttemptEntity::toDomain);
    }

    @Override
    public Attempt save(Attempt record) {
        return attemptDsRepository
                .saveAndFlush(new AttemptEntity(record))
                .toDomain();
    }

    @Override
    public List<Attempt> saveAll(Collection<Attempt> records) {
        return attemptDsRepository.saveAllAndFlush(
                records.stream().map(AttemptEntity::new).toList()
        ).stream().map(AttemptEntity::toDomain).toList();
    }

    @Override
    public Page<Attempt> findAll(int pageOffset, int pageSize, EvaluationResultSelector selector) {
        Page<AttemptEntity> found;
        if (selector == EvaluationResultSelector.ALL)
            found = attemptDsRepository.findAll(PageRequest.of(pageOffset, pageSize));
        else
            found = attemptDsRepository.findAllByIsValidSolution(
                    selector == EvaluationResultSelector.CORRECT,
                    PageRequest.of(pageOffset, pageSize)
            );

        return found.map(AttemptEntity::toDomain);
    }

    @Override
    public void delete(Long id) {
        attemptDsRepository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        attemptDsRepository.deleteAll();
    }
}
