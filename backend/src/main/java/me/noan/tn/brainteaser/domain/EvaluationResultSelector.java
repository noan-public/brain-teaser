package me.noan.tn.brainteaser.domain;

/**
 * Used for filtering attempts based on their results
 */
public enum EvaluationResultSelector {
    ALL,
    INCORRECT,
    CORRECT
}
