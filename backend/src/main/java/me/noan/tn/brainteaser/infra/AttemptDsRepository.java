package me.noan.tn.brainteaser.infra;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AttemptDsRepository extends JpaRepository<AttemptEntity, Long> {
    Page<AttemptEntity> findAllByIsValidSolution(boolean isValidSolution, Pageable pageable);
}
