package me.noan.tn.brainteaser.domain;

import org.springframework.data.domain.Page;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface AttemptRepository {
    Attempt create(Attempt record);

    boolean exists(Long id);

    Optional<Attempt> get(Long id);
    Attempt save(Attempt record);
    List<Attempt> saveAll(Collection<Attempt> records);
    Page<Attempt> findAll(int pageOffset, int pageSize, EvaluationResultSelector selector);
    void delete(Long id);
    void deleteAll();
}
