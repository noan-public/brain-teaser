package me.noan.tn.brainteaser.domain;

/**
 * Problem solving data
 *
 * @param count      number of found solutions
 * @param solvedInMs time that was necessary to find all the solutions
 */
public record FindAllSolutionsData(int count, double solvedInMs) {
}
