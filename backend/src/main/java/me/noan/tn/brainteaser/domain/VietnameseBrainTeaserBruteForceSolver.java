package me.noan.tn.brainteaser.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.SequencedCollection;
import java.util.stream.IntStream;

public final class VietnameseBrainTeaserBruteForceSolver extends Solver<Integer> {
    public VietnameseBrainTeaserBruteForceSolver() {
        super(new VietnameseBrainTeaserEvaluator());
    }

    /**
     * Find all solutions by trying every possible combinations
     */
    @Override
    protected void doSolve() {
        solutions = new ArrayList<>();
        findSolutions(new ArrayList<>(), solutions);
    }

    /**
     * Recursive solution finder
     * Generates all possible coefficients combinations and evaluate each of them
     * to find the correct ones.
     *
     * @param attempt   built attempt
     * @param solutions list of found solutions
     */
    void findSolutions(List<Integer> attempt, Collection<SequencedCollection<Integer>> solutions) {
        IntStream.rangeClosed(1, 9)
                .filter(it -> !attempt.contains(it))
                .forEach(it -> {
                            attempt.add(it);
                            if (attempt.size() < 9) {
                                // Only attempts with 9 coefficients are eligible for evaluation
                                findSolutions(attempt, solutions);
                            } else if (evaluator.evaluate(attempt) == EvaluationResult.CORRECT) {
                                solutions.add(new ArrayList<>(attempt));
                            }
                            attempt.removeLast();
                        }
                );
    }
}
