package me.noan.tn.brainteaser.domain;

public enum EvaluationResult {
    INCORRECT,
    CORRECT
}
