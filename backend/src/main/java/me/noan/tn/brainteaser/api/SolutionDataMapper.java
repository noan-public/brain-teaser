package me.noan.tn.brainteaser.api;

import jakarta.validation.Valid;
import me.noan.dev.tn_test.brain_teaser.api.server.model.SolutionsResponseDto;
import me.noan.tn.brainteaser.domain.FindAllSolutionsData;

public class SolutionDataMapper {
    public static SolutionsResponseDto toDto(@Valid FindAllSolutionsData model) {
        var dto = new SolutionsResponseDto();
        dto.setFoundCount(model.count());
        dto.setExecutionTimeInMs(model.solvedInMs());
        return dto;
    }
}
