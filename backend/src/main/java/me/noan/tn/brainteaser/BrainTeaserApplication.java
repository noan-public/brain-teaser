package me.noan.tn.brainteaser;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import me.noan.tn.brainteaser.domain.Attempt;
import me.noan.tn.brainteaser.domain.VietnameseBrainTeaserEvaluator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
public class BrainTeaserApplication {

    public static void main(String[] args) {
        // Configure domain
        Attempt.evaluator = new VietnameseBrainTeaserEvaluator();

        SpringApplication.run(BrainTeaserApplication.class, args);
    }

    @Bean
    public OpenAPI customOpenAPI(@Value("${springdoc.version:unknown}") String appVersion) {
        return new OpenAPI()
                .components(new Components())
                .info(new Info()
                        .title("Vietnamese Brain Teaser API")
                        .version(appVersion)
                );
    }
}
