package me.noan.tn.brainteaser.domain;

import jakarta.validation.Valid;
import me.noan.tn.brainteaser.domain.*;
import me.noan.tn.brainteaser.infra.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.validation.annotation.Validated;

@Validated
public class BrainTeaserService {
    AttemptRepository attemptRepository;

    public BrainTeaserService(AttemptRepository attemptRepository) {
        this.attemptRepository = attemptRepository;
    }

    public Attempt addNewAttempt(@Valid Attempt attempt) {
        return attemptRepository.create(attempt);
    }

    public Attempt getAttempt(Long id) {
        return attemptRepository.get(id)
                .orElseThrow(() -> new NotFoundException(String.format("Attempt with id=%d not found", id)));
    }

    public Page<Attempt> getAttemptPage(int pageOffset, int pageSize, EvaluationResultSelector selector) {
        return attemptRepository.findAll(pageOffset, pageSize, selector);
    }

    public Attempt updateAttempt(@Valid Attempt attempt) {
        return attemptRepository.save(attempt);
    }

    public void deleteAttempt(Long id) {
        if (!attemptRepository.exists(id))
            throw new NotFoundException(String.format("Attempt with id=%d not found", id));

        attemptRepository.delete(id);
    }

    public void deleteAll() {
        attemptRepository.deleteAll();
    }

    public FindAllSolutionsData findAllSolutions() {
        var solver = new VietnameseBrainTeaserBruteForceSolver();
        attemptRepository.saveAll(solver.solve().stream().map(Attempt::new).toList());
        return new FindAllSolutionsData(
                solver.getSolutions().size(),
                solver.getLastExecutionTimeInMs()
        );
    }
}
