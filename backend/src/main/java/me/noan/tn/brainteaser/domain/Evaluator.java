package me.noan.tn.brainteaser.domain;

import lombok.Getter;

import java.util.SequencedCollection;

/**
 * Allows to evaluate an attempt and retains the evaluation result.
 *
 * @param <T> type of the coefficients
 */
@Getter
public abstract class Evaluator<T> {
    EvaluationResult result;

    /**
     * Evaluates if an attempt is a solution or not.
     *
     * @param attempt list of coefficients that compose the attempt
     * @return CORRECT if attempt complies with expectations, INCORRECT else
     */
    public abstract EvaluationResult evaluate(SequencedCollection<T> attempt);
}
