package me.noan.tn.brainteaser.domain;

import lombok.Getter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.SequencedCollection;

/**
 * Allows to find all the solutions of a problem and retains found solutions
 * and time that needed to find them.
 *
 * @param <T> coefficients type
 */
public abstract class Solver<T> {
    protected Solver(Evaluator<T> evaluator) {
        this.evaluator = evaluator;
    }

    Evaluator<T> evaluator;
    @Getter
    private Double lastExecutionTimeInMs;
    @Getter
    protected Collection<SequencedCollection<T>> solutions;

    public Collection<SequencedCollection<T>> solve() {
        solutions = new ArrayList<>();
        var startTime = System.nanoTime();
        doSolve();
        var endTime = System.nanoTime();
        lastExecutionTimeInMs = (endTime - startTime) / 1000000.0;
        return solutions;
    }

    protected abstract void doSolve();
}
