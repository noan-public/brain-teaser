package me.noan.tn.brainteaser.infra;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

import java.util.Arrays;
import java.util.List;

import static java.util.Collections.emptyList;

/**
 * Allows to store an integer list as String in order to prevent using an extra data table.
 */
@Converter
public class IntegerListConverter implements AttributeConverter<List<Integer>, String> {
    private static final String SPLIT_CHAR = ";";

    @Override
    public String convertToDatabaseColumn(List<Integer> list) {
        if (list == null) return "";
        var stringList = list.stream().map(Object::toString).toList();
        return String.join(SPLIT_CHAR, stringList);
    }

    @Override
    public List<Integer> convertToEntityAttribute(String string) {
        if (string == null) return emptyList();
        var stringList = Arrays.asList(string.split(SPLIT_CHAR));
        return stringList.stream().map(Integer::parseInt).toList();
    }
}
