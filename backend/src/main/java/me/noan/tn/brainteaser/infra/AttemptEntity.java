package me.noan.tn.brainteaser.infra;

import jakarta.persistence.*;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import me.noan.tn.brainteaser.domain.Attempt;
import me.noan.tn.brainteaser.domain.EvaluationResult;
import org.springframework.data.domain.Page;

import java.util.LinkedHashSet;
import java.util.List;

/**
 * Attempt representation for storage
 */
@Entity
@Getter
public class AttemptEntity {
    protected AttemptEntity() {
        // Empty on purpose, constructor needed by JPA
    }

    public AttemptEntity(Attempt attempt) {
        update(attempt);
    }

    public void update(Attempt attempt) {
        this.coefficients = attempt.getCoefficients().stream().toList();
        isValidSolution = attempt.getEvaluationResult() == EvaluationResult.CORRECT;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Size(min = 9, max = 9)
    @Convert(converter = IntegerListConverter.class)
    private List<@Min(1) @Max(9) Integer> coefficients;

    private boolean isValidSolution;

    public Attempt toDomain() {
        return new Attempt(id, new LinkedHashSet<>(coefficients));
    }
}
