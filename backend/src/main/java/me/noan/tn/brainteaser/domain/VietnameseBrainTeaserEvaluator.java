package me.noan.tn.brainteaser.domain;

import org.springframework.stereotype.Component;

import java.util.SequencedCollection;

/**
 * Vietnamese Brain-Teaser Evaluator considering that operations priority apply
 * and computing is done using real numbers (in opposition to integers)
 */
@Component
public class VietnameseBrainTeaserEvaluator extends Evaluator<Integer> {
    @Override
    public EvaluationResult evaluate(SequencedCollection<Integer> attempt) {
        var c = attempt.stream().map(Integer::doubleValue).toArray(Double[]::new); // Convert to array
        var computed = c[0] + 13 * c[1] / c[2] + c[3] + 12 * c[4] - c[5] - 11 + c[6] * c[7] / c[8] - 10;
        result = computed == 66.0 ? EvaluationResult.CORRECT : EvaluationResult.INCORRECT;
        return result;
    }
}
