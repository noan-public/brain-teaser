package me.noan.tn.brainteaser.domain;

import org.springframework.stereotype.Component;

import java.util.SequencedCollection;

/**
 * Vietnamese Brain-Teaser Evaluator considering that operations priority do not apply
 * and computing is done using integers (division with no integer result are considered incorrect)
 */
@Component
public class VietnameseBrainTeaserOldEvaluator extends Evaluator<Integer> {
    @Override
    public EvaluationResult evaluate(SequencedCollection<Integer> attempt) {
        Integer[] coefficient = attempt.toArray(new Integer[0]);

        var computed = coefficient[0];
        computed += 13;
        computed *= coefficient[1];

        // Check that division result is an integer
        if (computed % coefficient[2] != 0)
            return result = EvaluationResult.INCORRECT;

        computed /= coefficient[2];
        computed += coefficient[3];
        computed += 12;
        computed *= coefficient[4];
        computed -= coefficient[5];
        computed -= 11;
        computed += coefficient[6];
        computed *= coefficient[7];

        // Check that division result is an integer
        if (computed % coefficient[8] != 0)
            return result = EvaluationResult.INCORRECT;

        computed /= coefficient[8];
        computed -= 10;

        result = computed == 66 ? EvaluationResult.CORRECT : EvaluationResult.INCORRECT;
        return result;
    }
}
