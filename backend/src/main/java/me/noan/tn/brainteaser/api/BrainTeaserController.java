package me.noan.tn.brainteaser.api;

import me.noan.dev.tn_test.brain_teaser.api.server.DefaultApi;
import me.noan.dev.tn_test.brain_teaser.api.server.model.*;
import me.noan.tn.brainteaser.domain.BrainTeaserService;
import me.noan.tn.brainteaser.domain.AttemptRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;


@RestController
public class BrainTeaserController implements DefaultApi {
    AttemptRepository attemptRepository;
    BrainTeaserService service;

    public BrainTeaserController(@Autowired AttemptRepository attemptRepository) {
        this.attemptRepository = attemptRepository;
        this.service = new BrainTeaserService(attemptRepository);
    }

    @Override
    public ResponseEntity<AttemptResponseDto> proposeSolution(AttemptRequestDto attemptRequestDto) {
        var model = service.addNewAttempt(AttemptMapper.toModel(attemptRequestDto));
        return ResponseEntity.status(CREATED)
                .body(AttemptMapper.toDto(model));
    }

    @Override
    public ResponseEntity<AttemptResponseDto> getAttempt(Long id) {
        var model = service.getAttempt(id);
        return ResponseEntity.status(OK)
                .body(AttemptMapper.toDto(model));
    }

    @Override
    public ResponseEntity<AttemptResponseDto> updateAttempt(Long id, AttemptRequestDto attemptRequestDto) {
        var model = service.updateAttempt(AttemptMapper.updateModel(id, attemptRequestDto));
        return ResponseEntity.status(OK)
                .body(AttemptMapper.toDto(model));
    }

    @Override
    public ResponseEntity<AttemptPageDto> getAttempts(PageQueryDto pageQuery,
                                                      AttemptResultFilterDte attemptResultFilter) {
        var modelPage = service.getAttemptPage(
                pageQuery.getPageIndex(),
                pageQuery.getPageSize(),
                AttemptMapper.toModel(attemptResultFilter)
        );
        return ResponseEntity.status(OK)
                .body(AttemptMapper.toDto(modelPage));
    }

    @Override
    public ResponseEntity<Void> deleteAttempt(Long id) {
        service.deleteAttempt(id);
        return ResponseEntity.status(OK).build();
    }

    @Override
    public ResponseEntity<Void> deleteAttempts() {
        service.deleteAll();
        return ResponseEntity.status(OK).build();
    }

    @Override
    public ResponseEntity<SolutionsResponseDto> computeSolutions() {
        var data = service.findAllSolutions();
        return ResponseEntity.status(OK)
                .body(SolutionDataMapper.toDto(data));
    }
}
