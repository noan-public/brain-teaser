package me.noan.tn.brainteaser.domain;

/**
 * Thrown when some application context is missing
 */
public class UninitializedContextException extends RuntimeException {
    public UninitializedContextException(String errorMessage) {
        super(errorMessage);
    }
}
