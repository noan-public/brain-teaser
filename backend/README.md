# Vietnamese Brain-Teaser Backend

## Design Rational & Explanations

This backend is implemented using SprinBboot MVC as it is very usual way (in opposite to SprinBboot reactive).

I followed clean architecture pattern in the way I isolated the business domain logic from underlying 
SpringBoot technology. (Not fully true as domain still depends on JPA `Page`. Some more work would be needed to finish this)

I also provided some abstraction in business domain logic implementation to showcase that domain could be easily
extended to support more than one problem kind.

> Note: mappers could have been impemented with [Mapstruct](https://mapstruct.org/) but I kept manual implementation here

## Run me

> Pre-requisite: build API server stubs

```bash
# From root project folder
./gradlew :backend:bootRun
```

[Swagger UI](http://localhost:8080/api/swagger-ui/index.html) is available when running.

# Tests

Some unit tests are available in the project.

```bash
# From root project folder
./gradlew :backend:test
```
