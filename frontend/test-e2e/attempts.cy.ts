describe('attempts spec', () => {
  it('displays the application title', () => {
    cy.visit('/')
    cy.get('[data-testId=app-title]').should('have.text', 'Vietnamese Brain-Teaser')
  })

  it('can add an attempt', () => {
    // GIVEN the main app page
    cy.visit('/')

    // WHEN I click on remove all attemps
    cy.get('[data-testId=remove-all-attempts]').click()
    // THEN table is emptied
    cy.get('[data-rowIndex=0]').should('not.exist')

    // WHEN I click on add new attemps
    cy.get('[data-testId=goto-add-attempts]').click()
    // THEN I land on attempt creation form 
    cy.get('[data-testId=create-attempt-form]').should('exist')
    //   AND submit button is disabled
    cy.get('[data-testId=submit-attempt]').should('be.disabled')
    //   AND 9 coeffs are present
    // WHEN I fill all coeffs with valid values
    var coeffs = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    for (var idx = 0; idx < coeffs.length; idx++) {
      cy.get(`[data-testId=attempt-coeff${idx}]`).should('exist')
      cy.get(`[data-testId=attempt-coeff${idx}]`).type(`${coeffs[idx]}`)
    }
    // THEN submit button is enabled
    var submit = cy.get('[data-testId=submit-attempt]')
    submit.should('be.enabled')

    // WHEN I submit the attempt
    submit.click()
    // THEN the new attempt appears in the table
    cy.get('[data-rowIndex=0]').should('exist')
  })
})
