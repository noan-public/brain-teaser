import { Check, Delete, DoNotDisturb, Edit } from "@mui/icons-material";
import { IconButton, Stack, Tooltip, Drawer, Box } from "@mui/material";
import { DataGrid, GridColDef, GridPaginationModel } from '@mui/x-data-grid';
import { ReactNode, useCallback, useContext, useEffect, useState } from "react";
import { BackendContext, getErrorMessage } from "../components/BackendProvider";
import { FeedbackContext } from "../components/FeedbackProvider";
import { AttemptResponseDto, AttemptResultFilterDte } from "@noandev/noan_tn_test_brain_teaser_api_sdk";
import AttemptFilterSelect from "../components/AttemptFilterSelect";
import AttemptsActionBar from "../components/AttemptsActionBar";
import AttemptCreationForm from "../components/AttemptCreationForm";
import AttemptEditionForm from "../components/AttemptEditionForm";
import FullHeightContainer from "../components/FullHeightContainer";

function Verdict({ isSuccess }: { isSuccess: boolean }) {
    var chip: ReactNode;
    if (isSuccess)
        chip = <Check color="success" aria-label="correct attempt" />
    else
        chip = <DoNotDisturb color="error" aria-label="incorrect attempt" />
    return (chip)
}

export default function AttemptsPage() {
    const backend = useContext(BackendContext)
    const feedback = useContext(FeedbackContext)
    const [loading, setLoading] = useState(true)
    const [filter, setFilter] = useState<AttemptResultFilterDte>(AttemptResultFilterDte.All)
    const [pageIndex, setPageIndex] = useState(0)
    const [pageSize, setPageSize] = useState(10)
    const [rows, setRows] = useState<AttemptResponseDto[]>([])
    const [rowCount, setRowCount] = useState(0)
    const [showEditionForm, setShowEditionForm] = useState(false)
    const [showCreationForm, setShowCreationForm] = useState(false)
    const [editId, setEditId] = useState(-1)

    const refresh = useCallback(() => {
        setLoading(true)
        backend.api.getAttempts({ pageQueryDto: { pageIndex, pageSize }, attemptResultFilter: filter })
            .then(
                response => {
                    setRows(response.attempts)
                    setRowCount(response.totalItemCount!)
                    setPageIndex(response.currentPageIndex!)
                },
                async error => {
                    var msg = await getErrorMessage(error)
                    feedback.display({ content: `Couldn't retrieve attempt list: ${msg}`, type: "error" })
                }
            )
            .finally(() => setLoading(false))
    }, [pageIndex, pageSize, filter, backend, feedback])

    useEffect(() => {
        refresh()
    }, [pageIndex, pageSize, filter, refresh]);

    // Actions
    function findAllSolutions() {
        backend.api.computeSolutions()
            .then(
                response => {
                    feedback.display({ content: `Found ${response.foundCount} solutions in ${response.executionTimeInMs} ms`, type: "success" })
                    refresh()
                },
                async error => {
                    var msg = await getErrorMessage(error)
                    feedback.display({ content: `Find all solutions failed: ${msg}`, type: "error" })
                }
            )
    }

    function removeAllRecords() {
        backend.api.deleteAttempts()
            .then(
                () => refresh(),
                async error => {
                    var msg = await getErrorMessage(error)
                    feedback.display({ content: `Remove all failed: ${msg}`, type: "error" })
                }
            )
    }

    function deleteRecord(recordId: number) {
        backend.api.deleteAttempt({ id: recordId })
            .then(
                _response => {
                    feedback.display({ content: `Removed attempts #${recordId}`, type: "success" })
                    refresh()
                },
                async error => {
                    var msg = await getErrorMessage(error)
                    feedback.display({ content: `Remove all failed: ${msg}`, type: "error" })
                }
            )
    }

    function addRecord() {
        setShowCreationForm(true)
    }

    function onAddRecordSucceed() {
        setShowCreationForm(false)
        refresh()
    }

    function onAddRecordCanceled() {
        setShowCreationForm(false)
    }

    function editRecord(recordId: number) {
        setEditId(recordId)
        setShowEditionForm(true)
    }

    function onEditRecordSucceed() {
        setEditId(-1)
        setShowEditionForm(false)
        refresh()
    }

    function onEditRecordCanceled() {
        setEditId(-1)
        setShowEditionForm(false)
    }

    // Filter & Pagination
    const changeFilter = (value: AttemptResultFilterDte) => {
        setFilter(value)
    }

    function onPaginationModelChange(model: GridPaginationModel) {
        if (loading) return; // Prevent enexpected behavior

        setPageSize(model.pageSize)
        setPageIndex(model.page)
    }

    // Widgets
    function SingleItemActionBar({ recordId }: { recordId: number }) {
        const removeDescription = 'Remove item'
        const editDescription = 'Edit item'
        return (
            <Stack direction="row">
                <Tooltip title={removeDescription}>
                    <IconButton onClick={() => deleteRecord(recordId)} aria-label={removeDescription}>
                        <Delete color="primary" />
                    </IconButton>
                </Tooltip>
                <Tooltip title={editDescription}>
                    <IconButton onClick={() => editRecord(recordId)} aria-label={editDescription}>
                        <Edit color="primary" />
                    </IconButton>
                </Tooltip>
            </Stack>
        )
    }

    const columns: GridColDef[] = [
        {
            field: 'actions', headerName: 'Actions', width: 80, renderCell: (cellValues) => {
                const recordId: number = cellValues.row.id
                return (<SingleItemActionBar recordId={recordId} />)
            },
            align: "center", headerAlign: "center"
        },
        {
            field: 'result', headerName: 'Verdict', width: 30, renderCell: (cellValues) => {
                return (<Verdict isSuccess={cellValues.row.result === "CORRECT"} />)
            },
            align: "center", headerAlign: "center"
        },
        { field: 'elements', headerName: 'Coefficients', width: 120 },
        { field: 'id', headerName: 'ID', width: 80,  },
    ];

    // Page
    return (
        <FullHeightContainer sx={{ p: 4 }}>
            <Stack mb='2' mr='2'
                direction='row' alignContent='baseline' flexWrap='wrap' justifyContent='space-between'>
                <AttemptsActionBar
                    onAdd={addRecord}
                    onFindAll={findAllSolutions}
                    onRemoveAll={removeAllRecords}
                    onRefresh={refresh} />
                <AttemptFilterSelect
                    onChanged={changeFilter} />
            </Stack>

            <DataGrid sx={{ my: 2, flex: '1 1 auto' }}
                autoHeight={false} rows={rows} columns={columns}
                paginationMode="server" sortingMode="server" filterMode="server"
                disableColumnMenu
                pageSizeOptions={[10, 25, 100]}
                loading={loading}
                onPaginationModelChange={(model) => onPaginationModelChange(model)}
                paginationModel={{ page: pageIndex, pageSize }}
                rowCount={rowCount}
            />

            <Drawer anchor='right'
                open={showCreationForm}
                onClose={() => onAddRecordCanceled()} >
                <Box maxWidth={700}>
                    <AttemptCreationForm
                        onSubmit={() => onAddRecordSucceed()}
                        onCancel={() => onAddRecordCanceled()} />
                </Box>
            </Drawer>
            <Drawer anchor='right'
                open={showEditionForm}
                onClose={() => onEditRecordCanceled()} >
                <Box maxWidth={700}>
                    <AttemptEditionForm
                        attemptId={editId}
                        onSubmit={() => onEditRecordSucceed()}
                        onCancel={() => onEditRecordCanceled()} />
                </Box>
            </Drawer>
        </FullHeightContainer>
    )
}
