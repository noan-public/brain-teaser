import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import type { } from '@mui/x-data-grid/themeAugmentation';
import BackendContextProvider from './components/BackendProvider';
import './index.css'
import FeedbackContextProvider from './components/FeedbackProvider';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

const defaultTheme = createTheme({
  // theme customization here
});

root.render(
  <React.StrictMode>
    <ThemeProvider theme={defaultTheme}>
      <BackendContextProvider apiUrl={new URL("http://localhost:8080/api")}>
        <FeedbackContextProvider>
          <App />
        </FeedbackContextProvider >
      </BackendContextProvider>
    </ThemeProvider>
  </React.StrictMode>
);
