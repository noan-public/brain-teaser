import { Add, AutoAwesome, Delete, Refresh } from "@mui/icons-material";
import { Button, ButtonGroup, Tooltip } from "@mui/material";

const addDescription = "Add new item"
const findAllDescription = "Find all solutions"
const removeAllDescription = "Remove all items"
const refreshDescription = "Refresh data"

export default function AttemptsActionBar({ onAdd, onFindAll, onRemoveAll, onRefresh }:
    {
        onAdd?: () => void,
        onFindAll?: () => void,
        onRemoveAll?: () => void,
        onRefresh?: () => void
    }
) {

    return (
        <ButtonGroup variant="contained" aria-label="attempt actions">
            <Tooltip title={addDescription}>
                <Button aria-label={addDescription} data-testid='goto-add-attempts'
                    onClick={onAdd}>
                    <Add />
                </Button>
            </Tooltip>  
            <Tooltip title={findAllDescription}>
                <Button aria-label={findAllDescription} data-testid='find-all-solutions'
                    onClick={onFindAll}>
                    <AutoAwesome />
                </Button>
            </Tooltip>
            <Tooltip title={removeAllDescription}>
                <Button aria-label={removeAllDescription} data-testid='remove-all-attempts'
                    onClick={onRemoveAll} color='warning'>
                    <Delete />
                </Button>
            </Tooltip>
            {false &&
                <Tooltip title={refreshDescription}>
                    <Button aria-label={refreshDescription} data-testid='refresh-visible-attempts'
                        onClick={onRefresh}>
                        <Refresh />
                    </Button>
                </Tooltip>
            }
        </ButtonGroup>
    )
}