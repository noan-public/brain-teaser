import { Alert, AlertColor, Snackbar, SnackbarCloseReason } from "@mui/material";
import { ReactNode, SyntheticEvent, createContext, useState } from "react";

export const FeedbackContext = createContext<FeedbackContextData>({
    display: (_message: FeedbackMessage) => { }
});

export type FeedbacktType = AlertColor;

interface FeedbackContextData {
    display: (message: FeedbackMessage) => void
}

interface FeedbackMessage {
    content: string,
    type: FeedbacktType
}

const FeedbackContextProvider = ({ children }: { children: ReactNode }) => {
    const [open, setOpen] = useState(false);
    const [message, setMessage] = useState<FeedbackMessage>({ content: "no message", type: 'info' });

    const handleClose = (
        _event?: SyntheticEvent | Event,
        reason?: SnackbarCloseReason) => {

        switch (reason) {
            case "clickaway": return
        }
        setOpen(false);
    };

    const displayFeedback = (message: FeedbackMessage) => {
        setMessage(message);
        setOpen(true)
        console.log(`${message.type}: ${message.content}`)
    }

    return (
        <FeedbackContext.Provider value={{ display: displayFeedback }}>
            {children}
            <Snackbar
                open={open}
                onClose={handleClose}
                autoHideDuration={3000}
            >
                <Alert
                    onClose={handleClose}
                    severity={message.type}
                    variant="filled"
                    sx={{ width: '100%' }}
                >
                    {message.content}
                </Alert>
            </Snackbar>
        </FeedbackContext.Provider>
    );
};

export default FeedbackContextProvider