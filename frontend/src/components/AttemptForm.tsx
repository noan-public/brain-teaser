import { Box, Button, Paper, TextField, Typography, styled } from "@mui/material";
import Grid from '@mui/material/Unstable_Grid2';
import { ChangeEvent, FormEvent, useEffect, useState } from "react";


// Static equation item component (operations & const numbers)
const StaticItem = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#ddd',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));

// Equation coefficient state type
interface InputState {
    value: number
    isValid: boolean
}

interface IValidationHandler {
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
    index: number
}

// Dynamis equation item component (coefficient)
interface EditableItemProps {
    index: number
    state: InputState
    validator: (data: IValidationHandler) => void
}

const DynItem = ({ index, state, validator }: EditableItemProps) => {
    return (
        <TextField
            data-testid={`attempt-coeff${index}`}
            inputProps={{ maxLength: 1 }}
            InputLabelProps={{ shrink: true }}
            sx={{ width: 40 }}
            size="small"
            color="primary"
            value={isNaN(state.value) ? '' : state.value}
            onChange={(e) => validator({ event: e, index })}
            error={!state.isValid}
            required />
    )
}

interface AttemptFormProps {
    initialValues?: number[]
    onSubmit: (elements: number[]) => void
}

// Equation solution attempt form
export default function AttemptForm({ initialValues, onSubmit }: AttemptFormProps) {
    const [coeffs, setCoeffs] = useState<number[]>(
        initialValues !== undefined ? initialValues : new Array(9).fill(NaN)
    );
    const [valids, setValids] = useState<boolean[]>(new Array(9).fill(false));
    const [isFormValid, setIsFormValid] = useState<boolean>(false);

    useEffect(() => {
        const nextValids = valids.slice();
        const reg = new RegExp("[1-9]");

        // Validate all coefficient
        coeffs.forEach((it, idx, all) => {
            const othersCoeffs = all.slice()
            othersCoeffs.splice(idx, 1)
            nextValids[idx] = reg.test(it.toString()) && !othersCoeffs.includes(it)
        })

        setValids(nextValids)
        setIsFormValid(!valids.includes(false))
    }, [coeffs, valids])

    const submitAttempt = async (event: FormEvent<HTMLFormElement>) => {
        // Preventing the page from reloading
        event.preventDefault();
        // Ensure form is valid
        if (!isFormValid) return

        onSubmit(coeffs)
    }

    const onValueChanged = ({ event, index }: IValidationHandler) => {
        const newValueStr = event.target.value
        const newValueInt = parseInt(newValueStr)

        // Set value to user input
        const nextCoeffs = coeffs.slice();
        nextCoeffs[index] = newValueInt
        setCoeffs(nextCoeffs);
    };

    const buildEditableItemProps = (coeffIndex: number): EditableItemProps => {
        return {
            index: coeffIndex,
            state: { value: coeffs[coeffIndex], isValid: valids[coeffIndex] },
            validator: onValueChanged,
        }
    }

    var helperText = <></>
    if (!isFormValid) {
        helperText =
            <Box sx={{ border: 1, borderRadius: 1, borderColor: 'error.main', p: 1, my: 2 }}
                display="flex" justifyContent="center">
                <Typography color={'error'}>
                    Coefficients must all be numbers from 1 to 9 and must all be different.
                </Typography>
            </Box>
    }

    return (
        <Box component="form" onSubmit={submitAttempt}>
            <Grid container sx={{ my: 2 }} alignItems="center"
                rowSpacing={1} columnSpacing={{ xs: 0.5, sm: 1, md: 2 }}>
                <Grid xs="auto">
                    <DynItem {...buildEditableItemProps(0)} />
                </Grid>
                <Grid xs="auto">
                    <StaticItem>+</StaticItem>
                </Grid>
                <Grid xs="auto">
                    <StaticItem>13</StaticItem>
                </Grid>
                <Grid xs="auto">
                    <StaticItem>x</StaticItem>
                </Grid>
                <Grid xs="auto">
                    <DynItem {...buildEditableItemProps(1)} />
                </Grid>
                <Grid xs="auto">
                    <StaticItem>:</StaticItem>
                </Grid>
                <Grid xs="auto">
                    <DynItem {...buildEditableItemProps(2)} />
                </Grid>
                <Grid xs="auto">
                    <StaticItem>+</StaticItem>
                </Grid>
                <Grid xs="auto">
                    <DynItem {...buildEditableItemProps(3)} />
                </Grid>
                <Grid xs="auto">
                    <StaticItem>+</StaticItem>
                </Grid>
                <Grid xs="auto">
                    <StaticItem>12</StaticItem>
                </Grid>
                <Grid xs="auto">
                    <StaticItem>x</StaticItem>
                </Grid>
                <Grid xs="auto">
                    <DynItem {...buildEditableItemProps(4)} />
                </Grid>
                <Grid xs="auto">
                    <StaticItem>-</StaticItem>
                </Grid>
                <Grid xs="auto">
                    <DynItem {...buildEditableItemProps(5)} />
                </Grid>
                <Grid xs="auto">
                    <StaticItem>-</StaticItem>
                </Grid>
                <Grid xs="auto">
                    <StaticItem>11</StaticItem>
                </Grid>
                <Grid xs="auto">
                    <StaticItem>+</StaticItem>
                </Grid>
                <Grid xs="auto">
                    <DynItem {...buildEditableItemProps(6)} />
                </Grid>
                <Grid xs="auto">
                    <StaticItem>x</StaticItem>
                </Grid>
                <Grid xs="auto">
                    <DynItem {...buildEditableItemProps(7)} />
                </Grid>
                <Grid xs="auto">
                    <StaticItem>:</StaticItem>
                </Grid>
                <Grid xs="auto">
                    <DynItem {...buildEditableItemProps(8)} />
                </Grid>
                <Grid xs="auto">
                    <StaticItem>-</StaticItem>
                </Grid>
                <Grid xs="auto">
                    <StaticItem>10</StaticItem>
                </Grid>
                <Grid xs="auto">
                    <StaticItem>=</StaticItem>
                </Grid>
                <Grid xs="auto">
                    <StaticItem>66</StaticItem>
                </Grid>
            </Grid>
            {helperText}
            <Box display="flex" justifyContent="center">
                <Button data-testid={`submit-attempt`}
                variant="contained" type="submit" disabled={!isFormValid}>
                    Submit
                </Button>
            </Box>
        </Box>
    )
}

