import styled from "@emotion/styled";
import { Box } from "@mui/system";

const FullHeightContainer = styled(Box)(() => ({
    'display': 'flex',
    'flex-direction': 'column',
    'height': '100%',
    'text-align': 'center',
    'border-collapse': 'collapse',
    'overflow-y': 'hidden',
}))

export default FullHeightContainer