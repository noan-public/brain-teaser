import { useContext, useEffect, useState } from "react";
import AttemptForm from "./AttemptForm";
import { BackendContext, getErrorMessage } from "./BackendProvider";
import { FeedbackContext } from "./FeedbackProvider";
import { Box, CircularProgress, Container, IconButton, Stack, Typography } from "@mui/material";
import { Close } from "@mui/icons-material";

export default function AttemptEditionForm({ attemptId, onSubmit, onCancel }:
    { attemptId: number, onSubmit?: () => void, onCancel?: () => void }) {
    const backend = useContext(BackendContext)
    const feedback = useContext(FeedbackContext)
    const [id, setId] = useState(-1)
    const [fetchFailed, setFetchFailed] = useState(false)
    const [loading, setLoading] = useState(true)
    const [initialValues, setInitialValues] = useState<number[]>(new Array(9).fill(NaN))

    useEffect(() => {
        if (attemptId === id || attemptId <= 0) return
        setId(attemptId)
        setLoading(true)
        backend.api.getAttempt({ id: attemptId })
            .then(
                response => {
                    setInitialValues(Array.from(response.elements!.values()))
                },
                async error => {
                    setFetchFailed(true)
                    var msg = await getErrorMessage(error)
                    feedback.display({ content: `Cannot find attempt ${attemptId}: ${msg}`, type: "error" })
                }
            ).finally(() => { setLoading(false) })
    }, [attemptId, backend, feedback, id])

    const onInternalSubmit = (coeffs: number[]) => {
        backend.api.updateAttempt({ id: attemptId, attemptRequestDto: { elements: new Set(coeffs) } }).then(
            response => feedback.display({
                content: `Attempt updated successfuly with ${response.result} result`,
                type: response.result === "CORRECT" ? 'success' : 'info'
            }),
            async error => {
                var msg = await getErrorMessage(error)
                feedback.display({ content: `Attempt submission failed: ${msg}`, type: "error" })
            }
        ).finally(() => {
            onSubmit && onSubmit()  // Inform parent
        })
    }

    return (
        <Container  data-testid='edit-attempt-form' sx={{ p: 4 }}>
            <Stack direction='row' alignItems='baseline'>
                <Typography data-testid='edit-attempt-form-title'
                    gutterBottom variant="h4">
                    Edit attempt
                </Typography>
                <Typography gutterBottom variant="h6" sx={{ marginLeft: 2 }} flexGrow='1'>
                    (Id: {attemptId})
                </Typography>
                <IconButton onClick={() => { onCancel && onCancel() }}>
                    <Close />
                </IconButton>
            </Stack>
            {loading ? (
                <Box display="flex" justifyContent="center">
                    <CircularProgress />
                </Box>
            ) : (
                fetchFailed ? (
                    <Box sx={{ border: 1, borderRadius: 1, borderColor: 'error.main', p: 1, my: 2 }}
                        display="flex" justifyContent="center">
                        <Typography variant="body1" color={'error'}>
                            Data couldn't be retrieved
                        </Typography>
                    </Box>
                ) : (
                    <AttemptForm
                        onSubmit={(coeffs) => onInternalSubmit(coeffs)}
                        initialValues={initialValues} />
                )

            )}
        </Container >
    )
}

