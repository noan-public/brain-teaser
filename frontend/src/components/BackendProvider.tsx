import { ReactNode, createContext } from "react";
import { DefaultApi } from "@noandev/noan_tn_test_brain_teaser_api_sdk/dist/apis/DefaultApi";
import { Configuration } from "@noandev/noan_tn_test_brain_teaser_api_sdk";

interface BackendContextData {
    api: DefaultApi
}

const defaultBackendContext: BackendContextData = ({
    api: new DefaultApi(new Configuration({ basePath: "http://localhost:8080" }))
});

export const BackendContext = createContext<BackendContextData>(defaultBackendContext);

export const getErrorMessage = async (err: any): Promise<string> => {
    var errorMessage = ""
    try {
        const apiResponse = await err.response.json();
        errorMessage = `${apiResponse.detail}`
    } catch (exception: any) {
        errorMessage = `${err.message}. Is the server running ?`
        console.log(`system exception: ${err}`)
    }
    return errorMessage;
}

const BackendContextProvider = ({ children, apiUrl }: { children: ReactNode, apiUrl: URL }) => {
    const api = new DefaultApi(new Configuration({ basePath: apiUrl.toString() }));

    return (
        <BackendContext.Provider value={{ api }}>
            {children}
        </BackendContext.Provider>
    );
};

export default BackendContextProvider
