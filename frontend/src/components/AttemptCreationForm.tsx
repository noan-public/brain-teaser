import { useContext } from "react";
import AttemptForm from "./AttemptForm";
import { BackendContext, getErrorMessage } from "./BackendProvider";
import { FeedbackContext } from "./FeedbackProvider";
import { Container, IconButton, Stack, Typography } from "@mui/material";
import { Close } from "@mui/icons-material";

export default function AttemptCreationForm({ onSubmit, onCancel }:
    { onSubmit?: () => void, onCancel?: () => void }
) {
    const backend = useContext(BackendContext)
    const feedback = useContext(FeedbackContext)

    const onInternalSubmit = (coeffs: number[]) => {
        backend.api.proposeSolution({
            attemptRequestDto: { elements: new Set(coeffs) }
        }).then(
            response => feedback.display({
                content: `Attempt submitted successfuly with ${response.result} result`,
                type: response.result === "CORRECT" ? 'success' : 'info'
            }),
            async error => {
                var msg = await getErrorMessage(error)
                feedback.display({ content: `Attempt submission failed: ${msg}`, type: "error" })
            }
        ).finally(() => {
            onSubmit && onSubmit()  // Inform parent
        })
    }

    return (
        <Container data-testid='create-attempt-form'  sx={{ p: 4 }}>
            <Stack direction='row'>
                <Typography data-testid='create-attempt-form-title'
                    gutterBottom variant="h4" flexGrow='1'>
                    Make a attempt
                </Typography>
                <IconButton onClick={() => { onCancel && onCancel() }}>
                    <Close />
                </IconButton>
            </Stack>
            <AttemptForm onSubmit={(coeffs) => onInternalSubmit(coeffs)} />
        </Container>
    )
}

