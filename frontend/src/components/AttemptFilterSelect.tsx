import { FormControl, InputLabel, MenuItem, Select, SelectChangeEvent } from "@mui/material"
import { AttemptResultFilterDte } from "@noandev/noan_tn_test_brain_teaser_api_sdk"
import { useState } from "react"

type OnAttemptFilterChanged = (value: AttemptResultFilterDte) => void

export default function AttemptFilterSelect({ onChanged }: { onChanged?: OnAttemptFilterChanged }) {
    const [value, setValue] = useState<AttemptResultFilterDte>(AttemptResultFilterDte.All)

    const onChangedInternal = (event: SelectChangeEvent) => {
        const value = event.target.value as AttemptResultFilterDte
        setValue(value)
        onChanged && onChanged(value) // Forward to parent
    }

    return (
        <FormControl sx={{ minWidth: 120 }}>
            <InputLabel id="attempt-select-label">Filter</InputLabel>
            <Select
                labelId="attempt-select-label"
                label="Filter"
                onChange={onChangedInternal}
                value={value}
                size="small"
            >
                <MenuItem value={AttemptResultFilterDte.All}>All</MenuItem>
                <MenuItem value={AttemptResultFilterDte.Correct}>Correct</MenuItem>
                <MenuItem value={AttemptResultFilterDte.Incorrect}>Incorrect</MenuItem>
            </Select>
        </FormControl>
    )
}