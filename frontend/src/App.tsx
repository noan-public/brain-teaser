import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';

import { AppBar, CssBaseline, Toolbar, Typography } from '@mui/material';
import { School } from '@mui/icons-material';
import AttemptsPage from './pages/AttemptsPage';
import { Box } from '@mui/system';
import FullHeightContainer from './components/FullHeightContainer';


function App() {
  return (
    <>
      <CssBaseline />
      <FullHeightContainer>
        <AppBar position="relative" sx={{ flex: '0 1 auto' }}>
          <Toolbar>
            <School sx={{ mr: 2 }} />
            <Typography variant="h6" color="inherit" noWrap data-testid='app-title'>
              Vietnamese Brain-Teaser
            </Typography>
          </Toolbar>
        </AppBar>
        <Box sx={{ height: '100%' }}>
          <AttemptsPage />
        </Box>
      </FullHeightContainer>
    </>
  );
}

export default App;
