import { expect, test } from 'vitest';
import { render, screen } from '@testing-library/react';
import "@testing-library/jest-dom";
import App from '../src/App';

test('app has a title', () => {
  render(<App />);
  const linkElement = screen.getByText(/Vietnamese Brain-Teaser/i);
  expect(linkElement).toBeInTheDocument();
});
