rootProject.name = "brain-teaser"

include("api")
include("backend", "api:spring-server")
include("api:sdk-ts")

dependencyResolutionManagement {
    repositories {
        mavenCentral()
    }
}
