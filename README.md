# Brain Teaser

Technical test for hiring process of <anonymous> company.

## Modalités

### Livrable

un zip avec le projet envoyé via wetransfer

### Environnement technique

Spring Boot / Angular ou React / H2 (bdd)

### Contexte

Ce casse-tête vietnamien a fait le tour d’internet en 2015. Donné initialement à des enfants de CE2,
des internautes se sont remués les méninges pour tenter de le résoudre. Y parviendrez-vous ?

**Énoncé du casse-tête**: compléter le tableau ci-dessous en utilisant des chiffres de 1 à 9 avec la
contrainte de ne pas pouvoir utiliser deux fois le même chiffre.

```math
a + 13 * b / d + e + 12 * f - g - 11 + h * i / j - 10 = 66
```

### Travail demandé

Développer une petite application web sous la forme d’une SPA qui permet de visualiser les résultats et
de les manipuler.

Sur cette application, l’utilisateur pourra :

* générer l’ensemble des solutions trouvées par l’algorithme
    * les solutions seront stockées en base de données~~~~
    * le temps de calcul de la génération sera affiché à l’utilisateur en retour de son action
* proposer une solution au problème
    * la solution proposée sera enregistrée en base de données
    * un flag permettra de mémoriser si la solution est correcte ou non
* visualiser l’ensemble des solutions enregistrées en base de données
* visualiser une des solutions enregistrées en base de données
* modifier une des solutions enregistrée en base de données
    * prendre en compte que la solution peut changer de statut (correcte/incorrecte)
* supprimer une solution de la liste des solutions
* supprimer toutes les solutions enregistrées en base de données

Pour répondre à ce problème, un algorithme de génération de solutions devra être implémenté.

Il est demandé que le back soit développé à l’aide de Spring Boot, à l’aide de l’ORM JPA. Utilisez H2
pour la base de données.
Le back-end devra exposer des API REST qui seront consommées par le front-end.
Le front-end devra être développé sous Angular ou React.

Si vous êtes à l’aise avec, l’écriture de quelques tests automatisés sera appréciée.

## Travail effectué

Le travail est découpé en 3 sous-projets:

* API: définition du contrat de communication entre le backend et le frontend au format OpenAPI. 
  De cette spécification sont générés les stubs du backend et le SDK pour le frontend.
  * [README](api/README.md)
* Backend: Springboot MVC REST service
  * [README](backend/README.md)
* Frontend: React user interface
  * [README](frontend/README.md)

> Le frontend suppose que le backend est accessible à l'adresse : http://localhost:8080

## Pre-requisites

* [Install SDKMAN](https://sdkman.io/install)
* Install a default JDK: `sdk install java 21.0.2-open`
* Modify SDKMAN configuration file `$HOME/.sdkman/etc/config` as following:

  ```diff
  - sdkman_auto_env=false
  + sdkman_auto_env=true
  ```
* Initialize project java version

  ```bash
  # In project root dir
  sdk env
  sdk install
  ```

## Build me

```bash
# Build API server stubs
./gradlew :api:spring-server:build
# Build API typescript SDK
./gradlew :api:sdk-ts:generate
cd api/sdk-ts/build/brain-teaser && npm i && cd -
# Build Backend
./gradlew :backend:build
# Build Frontend
cd frontend && yarn && yarn build && cd -
```

## Run me

```bash
# Start Backend on port 8080
./gradlew :backend:bootRun
# Start Frontend
cd frontend && serve -s dist && cd -
```
