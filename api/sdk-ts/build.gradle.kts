plugins {
    id("org.openapi.generator")
}

val licenseFilename: String by project.parent!!.extra
val changelogFilename: String by project.parent!!.extra
val specificationFilepath: String by project.parent!!.extra
val apiDomain: String by project.parent!!.extra
val apiDomainSpecFile: String by project.parent!!.extra
val commitHash: String by project.parent!!.extra
val apiVersionPathElement: String by project.parent!!.extra
val apiNamePathElement: String by project.parent!!.extra

val generationDir: String = layout.buildDirectory.get().toString()
val targetLabel: String = "sdk-ts"

tasks {
    val domainGenDir: String = "${generationDir}/$apiDomain"

    register<org.openapitools.generator.gradle.plugin.tasks.GenerateTask>("generate") {
        dependsOn(":api:aggregate")

        group = "generation"
        inputSpec.set("${specificationFilepath}/$apiDomainSpecFile")
        outputDir.set(domainGenDir)
        generatorName.set("typescript-fetch")
        version.set("${project.version}")

        configOptions.put("npmName", "@noandev/${apiNamePathElement}_sdk")
        configOptions.put("npmVersion", "0.1.0")
        configOptions.put("withInterfaces", "true")
        configOptions.put("legacyDiscriminatorBehavior", "false")
        configOptions.put("disallowAdditionalPropertiesIfNotPresent", "false")

        doFirst {
            delete("${generationDir}/$apiDomain")
        }
        doLast {
            File("${specificationFilepath}/$apiDomainSpecFile")
                .copyTo(File("$generationDir/$apiDomain/$apiDomainSpecFile"), overwrite = true)
            File(changelogFilename)
                .copyTo(File("$generationDir/$apiDomain/$changelogFilename"), overwrite = true)
            File(licenseFilename)
                .copyTo(File("$generationDir/$apiDomain/$licenseFilename"), overwrite = true)
        }
    }

    register("check") {
        group = "verification"
        dependsOn(":check-api")
    }

    register("assemble") {
        group = "build"
        dependsOn("generate")

        doLast {
            project.exec {
                workingDir = File(domainGenDir)
                executable("npm")
                args("install")
            }

            project.exec {
                workingDir = File(domainGenDir)
                executable("npm")
                args("run", "build")
            }
        }
    }
}