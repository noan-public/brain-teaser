# Vietnamese Brain-Teaser API

## Design Rational & Explanations

API is specified using OpenAPI specification standard as a single source of truth.

This allows to generate backend stubs and frontends SDKs and thus quite simplifies
communication layer integration.

## Generate

```bash
# Generate SpringBoot stubs
# (Run me from root project folder)
./gradlew :api:spring-server:build

# Generate typescript SDK
# (Run me from root project folder)
./gradlew :api:sdk-ts:generate
cd api/sdk-ts/build/brain-teaser && npm i && cd -
```
