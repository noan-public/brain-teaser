@file:Suppress("UNUSED_VARIABLE")
import java.io.ByteArrayOutputStream

plugins {
    val springbootVersion = "3.2.1"

    id("org.springframework.boot") version "3.2.1" apply false
    id("io.spring.dependency-management") version "1.1.4" apply false
    id("org.openapi.generator") version "7.2.0"
}

val specificationFilepath: String = "$projectDir"
val generatedDir: String = "${projectDir}/docs"

val originalSpecificationFilepath: String = "${projectDir}/specification"
val documentationDir: String = "${projectDir}/docs"
val buildDir: String = "${projectDir}/build"

version = "0.1.0"

val apiDomain = "brain-teaser"
val apiDomainSpecFile = "$apiDomain-api.yml"

allprojects {
    val myGroup = "me.noan.dev"
    val systemSubgroup = "tn-test"

    group = "$myGroup.$systemSubgroup"
    version = rootProject.version

    // TODO: extract path elements below from OpenAPI specification file
    val apiName by extra { "noan-$systemSubgroup-$apiDomain-api" }
    val apiNamePathElement by extra { apiName.replace('-', '_') }
    val apiDomain by extra { apiDomain }
    val apiDomainSpecFile by extra { apiDomainSpecFile }

    val licenseFilename by extra { "${projectDir}/LICENSE" }
    val changelogFilename by extra { "${projectDir}/CHANGELOG.md" }
    val buildDir: String = layout.buildDirectory.get().toString()
    val specificationFilepath by extra { "$buildDir/$apiDomain" }

    tasks {
        withType<org.openapitools.generator.gradle.plugin.tasks.GenerateTask> {
            outputs.upToDateWhen { false }
            outputs.cacheIf { false }
        }
    }
}

tasks {
    register<org.openapitools.generator.gradle.plugin.tasks.GenerateTask>("aggregate") {
        group = "generation"
        inputSpec.set("${originalSpecificationFilepath}/$apiDomainSpecFile")
        outputDir.set("${buildDir}/$apiDomain")
        generatorName.set("openapi-yaml")
        version.set("${project.version}")

        configOptions.put("outputFile", apiDomainSpecFile)
        configOptions.put("ensureUniqueParams", "true")
        configOptions.put("legacyDiscriminatorBehavior", "false")
        configOptions.put("disallowAdditionalPropertiesIfNotPresent", "false")
    }

    register<org.openapitools.generator.gradle.plugin.tasks.ValidateTask>("check-api") {
        group = "verification"
        inputSpec.set("${originalSpecificationFilepath}/$apiDomainSpecFile")
        recommend.set(true)
    }

    register("test") {
        // Do nothing
    }

    register("clean") {
        group = "build"
        doLast {
            delete(buildDir)
        }
    }

    register("generate") {
        group = "build"
        dependsOn("aggregate")
    }

    register("assemble") {
        group = "build"
        dependsOn("generate")
    }

    register("printVersion") {
        doLast {
            println(project.version)
        }
    }
}
