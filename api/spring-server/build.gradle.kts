// WARN: plugin must be declared in base gradle project !!!
plugins {
    id("org.openapi.generator")
    id("java")
    `java-library`
}

java(Action {
    withSourcesJar()
})

java.sourceCompatibility = JavaVersion.VERSION_17

configurations {
    compileOnly {
        extendsFrom(configurations.annotationProcessor.get())
    }
}

dependencies {
    val springBootVersion = "3.2.1"
    val springDocVersion = "1.7.0"

    implementation("org.springframework.boot:spring-boot-starter-web:$springBootVersion")
    implementation("org.springframework.boot:spring-boot-starter-validation:$springBootVersion")

    api("org.springdoc:springdoc-openapi-ui:$springDocVersion")

    implementation("com.fasterxml.jackson.core:jackson-databind:2.15.0")
    implementation("org.openapitools:jackson-databind-nullable:0.2.6")
}

val outputDir: String = layout.buildDirectory.get().toString()
val sourceDir: String = "${projectDir}/src"

val licenseFilename: String by project.parent!!.extra
val changelogFilename: String by project.parent!!.extra
val specificationFilepath: String by project.parent!!.extra
val apiDomain: String by project.parent!!.extra
val apiDomainSpecFile: String by project.parent!!.extra
val commitHash: String by project.parent!!.extra
val apiName: String by project.parent!!.extra
val apiNamePathElement: String by project.parent!!.extra

val generationDir: String = layout.buildDirectory.get().toString()
val domainGenDir = "${generationDir}/$apiDomain"
val targetLabel: String = "server-spring"

sourceSets {
    main {
        java {
            srcDir("$domainGenDir/src/main/java")
        }
        resources {
            srcDir("$domainGenDir/src/main/resources")
        }
    }
}

tasks {
    register<org.openapitools.generator.gradle.plugin.tasks.GenerateTask>("generate") {
        dependsOn(":api:aggregate")

        group = "generation"
        inputSpec.set("${specificationFilepath}/$apiDomainSpecFile")
        outputDir.set(domainGenDir)
        generatorName.set("spring")
        version.set("${project.version}")

        groupId.set("${project.group}")
        packageName.set("$apiName-server")
        invokerPackage.set("${project.group}")
        modelPackage.set("${project.group}.$apiDomain.api.server.model")
        apiPackage.set("${project.group}.$apiDomain.api.server")

        configOptions.put("artifactId", "$apiName-server")
        configOptions.put("groupId", "${project.group}")
        configOptions.put("artifactVersion", "${project.version}")

        configOptions.put("developerOrganization", "NoaN Dev")
        configOptions.put("developerName", "Renaud de Chivré")
        configOptions.put("developerEmail", "contact@dev.noan.me")

        configOptions.put("useSpringBoot3", "true")
        configOptions.put("documentationProvider", "springdoc")

        configOptions.put("library", "spring-boot")
        configOptions.put("interfaceOnly", "true")
        configOptions.put("delegatePattern", "true")
        configOptions.put("disallowAdditionalPropertiesIfNotPresent", "false")
        configOptions.put("legacyDiscriminatorBehavior", "false")
        configOptions.put("unhandledException", "true")
        configOptions.put("performBeanValidation", "true")
        configOptions.put("useBeanValidation", "true")
        configOptions.put("useSpringController", "true")
        configOptions.put("useTags", "true")

        doFirst {
            delete(domainGenDir)
        }
        doLast {
            File("${specificationFilepath}/$apiDomainSpecFile")
                .copyTo(
                    File("$generationDir/$apiDomain/src/main/resources/openapi/$apiDomainSpecFile"),
                    overwrite = true
                )
            File(changelogFilename)
                .copyTo(File("$generationDir/$apiDomain/$changelogFilename"), overwrite = true)
            File(licenseFilename)
                .copyTo(File("$generationDir/$apiDomain/$licenseFilename"), overwrite = true)
        }
    }

    named("check") {
        group = "verification"
        dependsOn(":api:check-api")
    }
    named("assemble") {
        group = "build"
        dependsOn("generate")
    }
    named("compileJava") { dependsOn("generate") }
    named("sourcesJar") { dependsOn("generate") }
    named("processResources") { dependsOn("generate") }
}

tasks.jar {
    manifest {
        attributes["Specification-Title"] = "$apiName"
        attributes["Specification-Version"] = "${project.version}"
        attributes["Specification-Vendor"] = "NoaN Dev"
        attributes["Implementation-Title"] = "$apiName"
        attributes["Implementation-Version"] = "${project.version}"
        attributes["Implementation-Vendor"] = "NoaN Dev"
    }
}
